package info.hccis.smcgrahm.stevemcanescamp;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
/**
 * @author Steven McGrath
 * @since 2/15/2016.
 *
 *  The Adapter for our RecyclerView which handled the display for each of the cards that are generated.
 */

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.CampViewHolder> {


    public static class CampViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView campIDText;
        TextView campDesc;
        ImageView img;
        Button tweetButton;
        Button detailButton;

        CampViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv);
            campIDText = (TextView)itemView.findViewById(R.id.camp_id);
            campDesc = (TextView)itemView.findViewById(R.id.camp_description);
            img=(ImageView)itemView.findViewById(R.id.canesImage);
            tweetButton=(Button)itemView.findViewById(R.id.tweet);
            detailButton=(Button)itemView.findViewById(R.id.detail);


        }
    }

    List<Camp> camps;

    RVAdapter(List<Camp> camps){
        this.camps = camps;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public CampViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_camp, viewGroup, false);
        CampViewHolder pvh = new CampViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(CampViewHolder campViewHolder,  int i) {
        //Appends the Messages for the camp
        String campMessage = "Camp ID: "+String.valueOf(camps.get(i).campId);
        String descMessage = "Camp Description: "+camps.get(i).description;

        //Establishes the current camp value as final variables so the camp can be passed through an Intent.
        final int j = i;
        final int k = i;

        //Sets the Onclick for the detail button to pass the intent to the new activity.
        campViewHolder.detailButton.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                final Camp tempCamp = camps.get(j);
                Log.d("Message", String.valueOf(tempCamp.getContactName()));
                Context context = v.getContext();
                Intent intent = new Intent(context, DetailActivity.class);
                intent.putExtra("camp",tempCamp);
                context.startActivity(intent);

            }
        });

        //Uses Twitter built in URL function to pass the Information to Twitter Directly
        campViewHolder.tweetButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                final Camp tempCamp2 = camps.get(k);
                String tweetID = String.valueOf(tempCamp2.getCampId());
                String tweetUrl = "https://twitter.com/intent/tweet?text=Check out Canes Camp " + tweetID + " at @HollandCollege &url="
                        + "http://www.hollandcollege.com/athletics/canes-camps/";
                Uri uri = Uri.parse(tweetUrl);



                v.getContext().startActivity(new Intent(Intent.ACTION_VIEW, uri));
            }
        });
        campViewHolder.campIDText.setText(campMessage);
        campViewHolder.campDesc.setText(descMessage);
        campViewHolder.img.setImageResource(camps.get(i).picID) ;




    }

    @Override
    public int getItemCount() {
        return camps.size();
    }

}
