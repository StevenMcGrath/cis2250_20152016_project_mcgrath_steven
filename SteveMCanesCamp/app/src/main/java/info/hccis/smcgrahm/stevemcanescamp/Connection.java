package info.hccis.smcgrahm.stevemcanescamp;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/*
 * Connection class which gets data from any URL.
 *
 * What is AsyncTask?
 *
 * An background UI thread handler, which prevents the Main thread from becoming hung with large task like data access.
 * For more information: http://developer.android.com/reference/android/os/AsyncTask.html
 *
 * What is HttpURLConnection?
 *
 * Simply put it is an network connection.
 * For more information: http://developer.android.com/reference/java/net/HttpURLConnection.html
 *
 */

public class Connection extends AsyncTask<String, String, String> {

    private String link;

    public Connection(String link) {
        this.link = link;
    }

    public Connection() {
    }

    /*
     * Method performs the network connection in background. The response will be collected in calling class.
     */
    @Override
    protected String doInBackground(String... params) {

        StringBuilder result = new StringBuilder(); //sets return string
        HttpURLConnection urlConnection = null; //sets connection
        try {
            URL url = new URL(link); //set url
            urlConnection = (HttpURLConnection) url.openConnection(); //set url connection
            urlConnection.setConnectTimeout(5000); // connect timeout after 5 seconds
            urlConnection.setReadTimeout(5000); // read timeout after 5 seconds
            urlConnection.connect(); //connect to url

            InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream()); //pass results to input stream
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream)); //read input stream
            
            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line); //pass reader data to string builder
            }
        } catch (MalformedURLException e) {
            Log.d("Malformed URL", e.toString());
        } catch (IOException e) {
            Log.d("IOException", e.toString());
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect(); //close connection
            }
        }
        Log.d("[ JSON RESULT ]: ", result.toString());
        return result.toString(); //return results as string
    }

    @Override
    protected void onPostExecute(String results) {
        //this method is meant for testing data (can't return values)
        Log.d("Post Execute Results", results);
    }
}
