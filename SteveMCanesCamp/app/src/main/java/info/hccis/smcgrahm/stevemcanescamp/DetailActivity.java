package info.hccis.smcgrahm.stevemcanescamp;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.util.Linkify;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

import info.hccis.smcgrahm.stevemcanescamp.info.hccis.smcgrahm.stevemcanescamp.sqlite.DBHelper;

/**
 * @author Steven McGrath
 * @since 2/15/2016.
 *
 *  This activity displays the details of each camp when it is requested from the home page, The camp for the activity is passed through an intent to get the specific details
 */

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Intent intent = getIntent();





        Camp tCamp = intent.getParcelableExtra("camp");

        TextView id = (TextView)findViewById(R.id.camp_detail_id);
        TextView capacity = (TextView)findViewById(R.id.camp_detail_capacity);
        TextView contact = (TextView)findViewById(R.id.camp_detail_contact);
        TextView descLong = (TextView)findViewById(R.id.camp_detail_description_long);
        TextView dateStart = (TextView)findViewById(R.id.camp_detail_date_start);
        TextView dateEnd = (TextView)findViewById(R.id.camp_detail_date_end);
        Button submitButton = (Button)findViewById(R.id.camp_detail_submit);
        EditText edit = (EditText)findViewById(R.id.camp_detail_note_enter);









        String cid = "Camp ID: "+String.valueOf(tCamp.getCampId());
        final int cidClick =  tCamp.getCampId();
        id.setText(cid);
        String cap = "Capacity: "+String.valueOf(tCamp.getCapacity());
        capacity.setText(cap);
        String cont = "Contact Name: "+ tCamp.getContactName();
        contact.setText(cont);
        String descL = "Camp Description: "+tCamp.getDescriptionLong();
        descLong.setText(descL);
        String dateS = "Start Date: "+tCamp.getStartDate();
        dateStart.setText(dateS);
        String dateE = "End Date: "+tCamp.getEndDate();
        dateEnd.setText(dateE);

        TextView phoneText = (TextView) findViewById(R.id.camp_detail_phonenumber);

        phoneText.setText(R.string.phoneNumber);
        phoneText.setAutoLinkMask(Linkify.PHONE_NUMBERS);

        submitButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                DBHelper dbHelper = new DBHelper(view.getContext(), null);



                EditText edit = (EditText) findViewById(R.id.camp_detail_note_enter);
                String newNote = String.valueOf(edit.getText());

                dbHelper.insertNote(newNote, cidClick);
            }
        });






    }
}
