package info.hccis.smcgrahm.stevemcanescamp;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;



/**
 *
 * @author Steven McGrath
 * @since 2/16/2016
 *
 * A class to access the webservice and retrieve the camps from the Json objects
 */
public class GsonUtil {

    private final String CAMP_REST_URL = "http://bjmac.hccis.info:8080/canes/rest/get/camps";


    Gson gson = new Gson();

    public GsonUtil() {

    }

    public List<Camp> getCampsFromRest() {
        List<Camp> camps = null;

        Connection conn = new Connection(CAMP_REST_URL);
        conn.execute("");
        try {
            JsonParser jsonParser = new JsonParser();
            JsonObject jsonObject = jsonParser.parse(conn.get()).getAsJsonObject();

            Object intervention = jsonObject.get("camp");
            if (intervention instanceof JsonArray) {
                Camp[] tempCampArray = gson.fromJson(jsonObject.get("camp"), Camp[].class);
                camps = new ArrayList<>(Arrays.asList(tempCampArray));

            } else if (intervention instanceof JsonObject) {
                Camp tempCamp = gson.fromJson(jsonObject.get("camp"), Camp.class);
                camps.add(tempCamp);

            }
        } catch (InterruptedException |ExecutionException | NullPointerException e) {
            e.printStackTrace();
        }

        Log.d("[ GsonUtil CAMP ] ", camps.toString());
        return camps;
    }


}
