package info.hccis.smcgrahm.stevemcanescamp;

/**
 * Created by Steve on 2/23/2016.
 */
public class Note {
    private int id;
    private String noteContent;
    private int campId;

    public Note(int id, String noteContent, int campId )
    {
        this.id = id;
        this.campId = campId;
        this.noteContent = noteContent;
    }

    public int getCampId() {
        return campId;
    }

    public void setCampId(int campId) {
        this.campId = campId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNoteContent() {
        return noteContent;
    }

    public void setNoteContent(String noteContent) {
        this.noteContent = noteContent;
    }
}
