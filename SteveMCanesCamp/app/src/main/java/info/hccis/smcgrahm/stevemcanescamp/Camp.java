package info.hccis.smcgrahm.stevemcanescamp;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author Steven McGrath
 * @since 2/15/2016.
 *
 *  This is a class to represent the Camp, it is Parcelable in order to pass it through an intent between activities
 */
public class Camp implements Parcelable {
    int campId;
    int capacity;
    int picID;
    String contactName;
    String description;
    String descriptionLong;
    String endDate;
    String startDate;

     Camp(int campId, int capacity, String contactName, String description, String descriptionLong, String endDate, String startDate, int picID) {
         this.campId = campId;
         this.capacity = capacity;
         this.contactName = contactName;
         this.description = description;
         this.descriptionLong = descriptionLong;
         this.endDate = endDate;
         this.startDate = startDate;
         this.picID = picID;
     }

     public Camp(Parcel in) {

         this.campId = in.readInt();
         this.capacity = in.readInt();
         this.contactName = in.readString();
         this.description = in.readString();
         this.descriptionLong = in.readString();
         this.endDate = in.readString();
         this.startDate = in.readString();
         this.picID = in.readInt();
     }

    /*
    * The default methods for Parcelables defined for camps.
    *
    *
    *
     */
    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<Camp> CREATOR = new Parcelable.Creator<Camp>() {
        public Camp createFromParcel(Parcel in) {
            return new Camp(in);
        }

        public Camp[] newArray(int size) {
            return new Camp[size];
        }
    };
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.campId);
        dest.writeInt(this.capacity);
        dest.writeString(this.contactName);
        dest.writeString(this.description);
        dest.writeString(this.descriptionLong);
        dest.writeString(this.endDate);
        dest.writeString(this.startDate);
        dest.writeInt(this.picID);
    }

    public int getCampId() {
        return campId;
    }

    public void setCampId(int campId) {
        this.campId = campId;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getPicID() {
        return picID;
    }

    public void setPicID(int picID) {
        this.picID = picID;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionLong() {
        return descriptionLong;
    }

    public void setDescriptionLong(String descriptionLong) {
        this.descriptionLong = descriptionLong;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

}