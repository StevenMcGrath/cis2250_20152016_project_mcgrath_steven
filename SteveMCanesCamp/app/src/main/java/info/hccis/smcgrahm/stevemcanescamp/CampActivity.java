package info.hccis.smcgrahm.stevemcanescamp;



import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Steven McGrath
 * @since 2/15/2016.
 *
 *  The mainpage Activity for the application, Uses a RecyclerView to display a unique card for each Camp from the Webservice.
 */

public class CampActivity extends Activity {

    private List<Camp> camps;
    private RecyclerView rv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.content_camp);

        rv=(RecyclerView)findViewById(R.id.rv);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);


        initializeData();
        initializeAdapter();
    }

    private void initializeData(){

        //Connects to the Webservice and displays each camp in a Cardview
        GsonUtil gsonUtil = new GsonUtil();
        camps =  gsonUtil.getCampsFromRest();

        for(int i = 0; i <camps.size();i++)
        {
            //As the image is not defined in the Webservice, adds the picture to each Camp object
            camps.get(i).setPicID(R.drawable.canescamplogo);
        }

    }

    private void initializeAdapter(){
        RVAdapter adapter = new RVAdapter(camps);
        rv.setAdapter(adapter);
    }

}
