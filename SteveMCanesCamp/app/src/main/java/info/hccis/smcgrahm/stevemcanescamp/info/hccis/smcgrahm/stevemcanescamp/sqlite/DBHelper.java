package info.hccis.smcgrahm.stevemcanescamp.info.hccis.smcgrahm.stevemcanescamp.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import info.hccis.smcgrahm.stevemcanescamp.Note;

/**
 * Created by Steve on 2/19/2016.
 */
public class DBHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "CampNotes";          // <=== Set to 'null' for an in-memory database
    public static final String TABLE_NAME = "Notes";
    public static final String COLUMN_ID = "_id";           // <=== This is required
    public static final String COLUMN_NOTE = "note";
    public static final String COLUMN_CAMPID = "campid";

    private static final int DB_VERSION = 1;

    private static final String SQL_CREATE_TABLE_NOTE = "CREATE TABLE " + TABLE_NAME + "( " +
            COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_NOTE + " TEXT, " +
            COLUMN_CAMPID + " INT);";



    private SQLiteDatabase db;
    private Cursor cursor;


    public DBHelper(Context context, SQLiteDatabase.CursorFactory factory, DatabaseErrorHandler errorHandler) {
        super(context, DATABASE_NAME, factory, DB_VERSION, errorHandler);
    }

    public DBHelper(Context context, SQLiteDatabase.CursorFactory factory) {
        super(context, DATABASE_NAME, factory, DB_VERSION);
    }

    public boolean insertNote(String note, int campID) {
        boolean success;

        try {
            db = this.getWritableDatabase();
            ContentValues noteValues = new ContentValues();


            noteValues.put("note", note);
            noteValues.put("campid", campID);


            db.insert(TABLE_NAME, null, noteValues);

            success = true;
        } catch ( SQLiteException ex ) {
            Log.d("Note", "Failed to insert into the database");
            success = false;
        }

        return success;
    }


    public boolean updateNote(int id, String note, int campID) {
        boolean success;

        try {
            db = this.getWritableDatabase();
            ContentValues noteValues = new ContentValues();


            noteValues.put("note", note);
            noteValues.put("campid", campID);


            db.update(TABLE_NAME, noteValues, COLUMN_ID + "=" + id, null);

            success = true;
        } catch( SQLiteException ex ) {
            Log.d("Note", "Failed to update the record");
            success = false;
        }

        return success;

    }

    public boolean deleteNote(int id) {
        boolean success;

        try {
            db = this.getWritableDatabase();
            db.delete(TABLE_NAME, COLUMN_ID + "=" + id, null);

            success = true;

        } catch( SQLiteException ex ) {
            Log.d("Note", "Failed to delete the record");
            success = false;
        }
        return success;

    }

    public Cursor getNoteCursor() {

        ArrayList<Note> notes = new ArrayList<>();

        try {
            db = this.getReadableDatabase();
            //db.query(TABLE_NAME, new String[] {COLUMN_NAME, COLUMN_WEIGHT}, COLUMN_NAME+"=?", )
            cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
            cursor.moveToFirst();

        } catch( SQLiteException ex ) {
            Log.d("DeliverableDAO", "Failed to retrieve deliverables cursor");
        }


        return cursor;

    }

    public ArrayList<Note> getNotes() {

        ArrayList<Note> notes = new ArrayList<>();

        try {
            db = this.getReadableDatabase();
            //db.query(TABLE_NAME, new String[] {COLUMN_NAME, COLUMN_WEIGHT}, COLUMN_NAME+"=?", )
            cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);

            if(cursor.moveToFirst()) {
                do {
                    int noteId = cursor.getInt(0);
                    String note = cursor.getString(1);
                    int campId = cursor.getInt(2);
                    notes.add(new Note(noteId, note, campId));
                } while( cursor.moveToNext());
            }

        } catch( SQLiteException ex ) {
            Log.d("Note", "Failed to retrieve deliverables");
        }

        return notes;

    }

    public Note getNote(int id){

        Note note = null;
        db = this.getReadableDatabase();
        cursor =  db.rawQuery( "select * from " + TABLE_NAME + " where " + COLUMN_ID + "="+id+"", null );
        if(cursor.moveToFirst()) {

            note = new Note(cursor.getInt(0), cursor.getString(1), cursor.getInt(2));

        }
        return note;
    }

    public void closeConnection() {

        if( cursor != null) { cursor.close(); }
        if( db != null) { db.close(); }

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE_NOTE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
